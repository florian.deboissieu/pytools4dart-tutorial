This repository contains the notebooks for the pytools4dart tutorial. 
It has been updated for DART v1329 and pytools4dart v1.1.20.

The examples and user guides used as notebooks were extracted from pytools4dart repository:
- https://gitlab.com/pytools4dart/pytools4dart/-/tree/master/pytools4dart/examples
- https://gitlab.com/pytools4dart/pytools4dart/-/tree/master/docs/user_guides


# Install pytools4dart

If no conda is installed, install [Miniforge3](https://github.com/conda-forge/miniforge#miniforge3)

For Win users, open Miniforge from Start Menu.

If conda is installed but not Mamba, install it:
__Not recommended by miniforge but you can try__
```shell
conda install mamba -n base -c conda-forge
```

Install pytools4dart environement and activate it:
```shell
mamba env create -n myptd -f https://gitlab.com/pytools4dart/pytools4dart/-/raw/master/environment.yml
conda activate myptd
```

Download DART_v1341 (stable):
- Linux:
    - https://dart.omp.eu/membre/downloadDart/contenu/DART/Linux/64bits/DART_5-9-8_2023-08-04_v1341_linux64.tar.gz
    - https://nextcloud.inrae.fr/s/5gG3zwYSkY4KmJS
- Windows:
    - https://dart.omp.eu/membre/downloadDart/contenu/DART/Windows/64bits/DART_5-9-8_2023-08-04_v1341_windows64.zip
    - https://nextcloud.inrae.fr/s/T5s9kNbKkd94ssS

It can return a warning on the default obj reader, but no error.

Notebooks can be downloaded cloned and __enter the directory__ with the following:
```shell
git clone https://forgemia.inra.fr/florian.deboissieu/pytools4dart-tutorial.git
cd pytools4dart-tutorial
```

Start a python session and install DART (or configure `pytools4dart`) with your DART version:
```python
from path import Path
import pytools4dart as ptd
dart_zip = "/home/boissieu/git/pytools4dart-tutorial/DART_5-9-8_2023-08-04_v1341_linux64.tar.gz"# r'<path to DART zip>'
dart_dir = Path(r"DART_v1341").abspath()
user_data_dir = Path(r"user_data").abspath()
ptd.dart.install(dart_zip, dart_dir, user_data_dir)

# or if you alread have a DART installed
ptd.configure(r'<path to DART directory>') # e.g. r"~/DART", r"C:\DART"

# and exit
exit()
```



# Add R (optional)

Simulations can also be scripted in R, such as in `examples/use_case_0.R`.
In order to do so, R and some R packages must be added to the environment `myptd`. It can be done with the following:
```shell
mamba env update -n myptd -f https://forgemia.inra.fr/florian.deboissieu/pytools4dart-tutorial/-/raw/main/environment-R.yml
```

# Add Jupyter lab (optional)

If you are not used to a particular python IDE (e.g. pycharm, vscode), 
the simplest way to browse the use cases and the user guides while praticing would be through jupyterlab.

In order to have jupyterlab added to your existing `pytools4dart` envrionment:
```shell
mamba env update -n myptd -f https://forgemia.inra.fr/florian.deboissieu/pytools4dart-tutorial/-/raw/main/environment-lab.yml
```

Run Jupyter-lab with:
```shell
jupyter lab --ip 0.0.0.0
```
and `Ctrl+click` on the generated link `http://127.0.0.1:8888/lab?token=xxxx`.

Within jupyter-lab, in order to open a python scripts or a markdown files as jupyter notebooks,
right click on the file and choose `Open with ... > Notebook`.


# Troubleshooting

For some use cases run in a jupyter notebook or jupyter-lab, there may be an error raised with message:
```
ModuleNotFoundError: No module named 'matplotlib_inline'
```

This issue was notified in https://gitlab.com/pytools4dart/pytools4dart/-/issues/35. It can be solved installing the missing packages, as presented in the issue comments. The [solution coded in python](https://gitlab.com/pytools4dart/pytools4dart/-/issues/35#note_1436430737) can be executed from inside a jupyter notebook.

